#ifndef WIDE_FACTORY_SERVEUR_LOCAL_TRIGO_H
#define WIDE_FACTORY_SERVEUR_LOCAL_TRIGO_H

#include <k4a/k4a.h>
#include <k4abt.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <list>


/**
 * Used to wrap x and y axis in float
 */
typedef struct _POINTFLOAT
{
    float x;
    float y;
} POINTFLOAT, *PPOINTFLOAT;


float computeAngle( k4abt_joint_t a , k4abt_joint_t b, k4abt_joint_t c );
bool compareFrame(k4abt_skeleton_t skeleton, k4abt_skeleton_t referenceSkeleton);
bool detectInitialPosition(k4abt_skeleton_t skeleton, k4abt_skeleton_t referenceSkeleton);
std::vector<float> getAngles(k4abt_skeleton_t skeleton);
bool compareAngle(std::vector<float> oldAngle,std::vector<float> newAngle);
float getangle(k4abt_skeleton_t skeleton);
float getMinAngle();
std::string getResult(float angleDiffrence);
std::string getTimeResult(int frame);
float getTime();

#endif
