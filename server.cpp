#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <memory>
#include <fstream>

#include <uWebSockets/App.h>
#include <jsoncpp/json/writer.h>
#include <k4a/k4a.h>
#include <k4abt.h>
#include <bitset>
#include <chrono>

#include "kinect.h"
#include "movement.h"
#include "databaseManager.h"
#include "comparison.h"

using namespace std;
using namespace chrono;


void sendMovementFlux(auto* ws, int nbFrame, Kinect* kinect, uWS::OpCode opCode);
void sendMovementBloc(auto* ws, int nbFrame, Kinect* kinect, uWS::OpCode opCode);
void parser(auto* ws, Kinect* kinect, string_view message, uWS::OpCode opCode);

void compareMovement(auto* ws, Kinect* kinect, uWS::OpCode opCode);

/*
    function to create the server and enter it's main loop
    this is a blocking function
*/
void serveur(){

        struct ServerData //here we store everything that we need to use in the context function
        {
            Kinect* kinect;
        };

        uWS::App().get("/monTest", [](auto*res, auto*req)
        {
            /* You can efficiently stream huge files too */
            res->writeHeader("Content-Type", "text/html; charset=utf-8")->end("Hello HTTP!");
        }).ws<ServerData>("/monTest", {

                //context function

                /* Just a few of the available handlers */
                .open = [](auto*ws) //called on each new connection, we create the Kinect
                {
                    /* MQTT syntax */
                    ws->subscribe("sensors/+/house");
                    std::cout << "WS ouvert" << std::endl;
                    static_cast<ServerData*>(ws->getUserData())->kinect = Kinect::kinectFactory();
                },
                //Message to send
                .message = [](auto*ws, std::string_view message, uWS::OpCode opCode) { //called everytime we receive a new message, xcal the parser
                    Kinect* kinect = static_cast<ServerData*>(ws->getUserData())->kinect;
                    parser(ws, kinect, message,opCode);

                    // compareMovement(ws, kinect, opCode);
                },
                .close = [](auto *ws, int /*code*/, std::string_view /*message*/) //called when a connection is closed, we redease the kienct
                {
                    delete static_cast<ServerData*>(ws->getUserData())->kinect;
                    std::cout << "WS fermée" << std::endl;
                }
        }).listen(9001, [](auto*listenSocket) { //socket listen option

            if (listenSocket)
            {
                std::cout<<"Listening on port "<<9001<<std::endl;
            }
        }).run(); //everything after here should not execute as we are locked in the serv

}



// In global enbale to stop in compareMovement
bool exerciseFinished = false;


/** Running server */
int main()
{
    serveur();
    return EXIT_SUCCESS;
}


/**
 * function to send the movement one frame at a time, based on a timer for the stopping condition
 * @param ws used to send message
 * @param duration record duration
 * @param kinect used to command kinect get captured skeleton
 * @param opCode opcode
 */
void sendMovementFlux(auto* ws, int duration, Kinect* kinect, uWS::OpCode opCode)
{
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    while (duration_cast<milliseconds>(end - start).count() < duration*1000)
    {
        Movement movement = Movement();
        movement.addSkeleton(kinect->captureFrame());
        string json = movement.convertToJson(0 );

        std::cout<<kinect->getNumBodies()<<std::endl;
        //Stop processing data or collecting it once there's no body detected (output = null or output = {})
        // Send the data (String) frame by frame to the java client via ws
        if ( kinect->getNumBodies() > 0 )
        {
                  ws->send( json, opCode);
        }
        end = high_resolution_clock::now();
    }
}


/**
 * Compare a referenced movement with a real time movement
 * @param ws used to send message
 * @param kinect used to command kinect get captured skeleton
 * @param opCode opcode
 */
void compareMovement(auto* ws, Kinect* kinect, uWS::OpCode opCode)
{
    Movement s = getMovementSkeletons("curl");
    float minAngleRefrence = getMinAngle();
    cout<<"min angle"<<minAngleRefrence;
    float oldvalue = getangle(s.getSkeletons()[0]);
    // Exercise finished when all frames are passed
    while(!exerciseFinished)
    {
        k4abt_skeleton_t skeleton = kinect->captureFrame();
        Movement m = getMovementSkeletons("curl");
        // Start comparison only if the first frame matched with the first frame of referenced movement
        if(detectInitialPosition(skeleton, m.getSkeletons()[0]))
        {
            // Used to count frame passed for movement
            int frameCounterGeneral = 0;

            // Used to count frame captured for one frame (compute error percentage for one frame)
            int frameCounter = 0;
            int errorCounter = 0;

            float minAngle = 999;
            float maxAngle = 0;

            // Percentage general of error
            float errorPercentageGeneral = 0;
            int test=0;
            // Finish the comparison when all frame are passed
            while(1)
            {
                skeleton = kinect->captureFrame();
                if(oldvalue - getangle(skeleton)<-15) {
                        //cout << "positive" << endl;
                        if(detectInitialPosition(skeleton, m.getSkeletons()[0])){
                            cout<<"min angle refrence "<<minAngleRefrence<<" min angle "<<minAngle<<endl;
                            cout<<getResult(minAngle - minAngleRefrence)<<endl;
                            cout<<getTimeResult(frameCounterGeneral)<<endl;
                            cout<<"movement finished"<<endl ;
                            break;
                        }

                } else if (oldvalue - getangle(skeleton)>15) {
                   if (minAngle > getangle(skeleton)) minAngle = getangle(skeleton);
                    //cout << "negative" << endl;
                }

                oldvalue = getangle(skeleton);
                frameCounterGeneral += 1;
            }
            exerciseFinished = true;
        }
    }
}



/**
 * function to send the whole movement in on go
 * Send a movement recorded in one bloc (all frames)
 * @param ws used to send message
 * @param duration record duration
 * @param kinect used to command kinect get captured skeleton
 * @param opCode opcode
 */
void sendMovementBloc(auto* ws, int duration, Kinect* kinect, uWS::OpCode opCode)
{
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    Movement movement = Movement();
    while (duration_cast<milliseconds>(end - start).count() < duration*1000)
    {
        movement.addSkeleton(kinect->captureFrame());
        end = high_resolution_clock::now();
    }

    string json = movement.convertToJson(0 );
    ws->send( json, opCode);
}

/**
 * Parse message received and can record a movement
 * @param ws used to send message
 * @param message message received
 * @param kinect used to command kinect get captured skeleton
 * @param opCode opcode
 */
void parser(auto* ws, Kinect* kinect, string_view message, uWS::OpCode opCode){
    int nbFrames = 0;
    std::string_view delimiter = " ";
    size_t pos = 0;
    std::string_view token;

    pos = message.find(delimiter);
    token = message.substr(0, pos);
    message.remove_prefix(pos + delimiter.length());
    if (token == "r") { //recording mode, we send the full movement in one go
        pos = message.find(delimiter);
        token = message.substr(0, pos);
        nbFrames = (int) (std::atof(token.data()));

        sendMovementBloc(ws, nbFrames, kinect,opCode);
    }

}
