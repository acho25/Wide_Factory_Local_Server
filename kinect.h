#ifndef KINECT_H
#define KINECT_H

#include <k4a/k4a.h>
#include <k4abt.h>

/*
 * function that takes a task and an error message and shows this error message in case the task failed
 */
#define VERIFY(result, error)                                                                            \
    if(result != K4A_RESULT_SUCCEEDED)                                                                     \
    {                                                                                                    \
        printf("%s \n - (File: %s, Function: %s, Line: %d)\n", error, __FILE__, __FUNCTION__, __LINE__); \
        exit(1);                                                                                         \
    }                                                                                                    
/*
 * table that contains joints names
 */

/*
    wraper around the kinect to manipulate it safely
*/
class Kinect
{
private:
	k4a_device_t kinect;
	k4abt_tracker_t tracker;
    size_t num_bodies;

public:
	Kinect(k4a_device_t, k4abt_tracker_t);
	~Kinect();

	k4abt_skeleton_t captureFrame();
	static Kinect* kinectFactory();
    size_t getNumBodies();



};

#endif
