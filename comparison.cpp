#include "comparison.h"
#include "movement.h"
#include "databaseManager.h"



/** The enum of useful joint to do the comparison */
k4abt_joint_id_t jointname[16] =
{
    K4ABT_JOINT_PELVIS,
    K4ABT_JOINT_SPINE_NAVEL,
    K4ABT_JOINT_SHOULDER_LEFT,
    K4ABT_JOINT_ELBOW_LEFT,
    K4ABT_JOINT_WRIST_LEFT,
    K4ABT_JOINT_SHOULDER_RIGHT,
    K4ABT_JOINT_ELBOW_RIGHT,
    K4ABT_JOINT_WRIST_RIGHT,
    K4ABT_JOINT_HIP_LEFT,
    K4ABT_JOINT_KNEE_LEFT,
    K4ABT_JOINT_ANKLE_LEFT,
    K4ABT_JOINT_HIP_RIGHT,
    K4ABT_JOINT_KNEE_RIGHT,
    K4ABT_JOINT_ANKLE_RIGHT,
    K4ABT_JOINT_HEAD,
};


/**
 * Compute angle formed by 3 points
 * @param a first point
 * @param b second point
 * @param c third point
 * @return angle value
 */
float computeAngle(k4abt_joint_t a , k4abt_joint_t b, k4abt_joint_t c)
{
    float bx = b.position.xyz.x;
    float by = b.position.xyz.y;

    float ax = a.position.xyz.x;
    float ay = a.position.xyz.y;

    float cx = c.position.xyz.x;
    float cy = c.position.xyz.y;

    POINTFLOAT ab = {  bx - ax, by - ay };
    POINTFLOAT cb = { bx - cx, by - cy };

    float dotProduct = (ab.x * cb.x + ab.y * cb.y);
    float cross = (ab.x * cb.y - ab.y * cb.x);

    float alpha = atan2(cross, dotProduct);

    return floor(alpha * 180. / 3.14 + 0.5);
}

/**
 * Get a list of angles of the wanted joints
 * @param skeleton skeletons with all joint
 * @return list of angles
 */
std::vector<float> getAngles(k4abt_skeleton_t skeleton)
{
    std::vector<float> anglesList ;

    // Get allwanted joints in skeleton
    k4abt_joint_t pelvis = skeleton.joints[jointname[0]];
    k4abt_joint_t spine = skeleton.joints[jointname[1]];
    k4abt_joint_t shoulder_left = skeleton.joints[jointname[2]];
    k4abt_joint_t elbow_left = skeleton.joints[jointname[3]];
    k4abt_joint_t wrist_left = skeleton.joints[jointname[4]];
    k4abt_joint_t shoulder_right = skeleton.joints[jointname[5]];
    k4abt_joint_t elbow_right = skeleton.joints[jointname[6]];
    k4abt_joint_t wrist_right = skeleton.joints[jointname[7]];
    k4abt_joint_t hip_left = skeleton.joints[jointname[3]];
    k4abt_joint_t knee_lrft = skeleton.joints[jointname[3]];
    k4abt_joint_t ankle_left = skeleton.joints[jointname[3]];
    k4abt_joint_t hip_right = skeleton.joints[jointname[3]];
    k4abt_joint_t knee_right = skeleton.joints[jointname[3]];
    k4abt_joint_t ankle_right = skeleton.joints[jointname[3]];
    k4abt_joint_t head = skeleton.joints[jointname[3]];

    // Compte angles between trio joints wanted
    float a1 = computeAngle(head, shoulder_right, elbow_right);
    float a2 = computeAngle(shoulder_right, elbow_right, wrist_right);
    float a3 = computeAngle(shoulder_right, head, spine);

    float a4 = computeAngle(head, shoulder_left, elbow_left);
    float a5 = computeAngle(shoulder_left, elbow_left, wrist_left);
    float a6 = computeAngle(shoulder_left, head, spine);

    float a7 = computeAngle(head, spine, hip_right);
    float a8 = computeAngle(head, spine, hip_left);

    float a9 = computeAngle(spine, hip_left, knee_lrft);
    float a10 = computeAngle(spine, hip_right, knee_right);

    float a11 = computeAngle(pelvis, hip_left, knee_lrft);
    float a12 = computeAngle(pelvis, hip_right, knee_right);

    float a13 = computeAngle(hip_left, knee_lrft, ankle_left);
    float a14 = computeAngle(hip_left, knee_lrft, ankle_left);


    // Add in a list the angles, some of there are commented because actually the comparison is optimised for curl movement

    //anglesList.push_back(a1);
    anglesList.push_back(a2);
    //anglesList.push_back(a3);
    //anglesList.push_back(a4);
    anglesList.push_back(a5);
    //anglesList.push_back(a6);
/*  anglesList.push_back(a7);
    anglesList.push_back(a8);
    anglesList.push_back(a9);
    anglesList.push_back(a10);
    anglesList.push_back(a11);
    anglesList.push_back(a12);
    anglesList.push_back(a13);
    anglesList.push_back(a14);
*/

    return anglesList;
}

/**
 * Compare two angles to see wich one is bigger
 * this will be used to determine the direction of a movement
 * @param oldAngle
 * @param newAngle
 * @return
 */

bool compareAngle(std::vector<float> oldAngle,std::vector<float> newAngle)
{
    for(int i = 0 ; i< newAngle.size();i++)
    {
        if(abs(oldAngle[i]) > abs(newAngle[i]))
        {
            return true;
        }
    }
    return false;
}


/**
 *return the angle between the shoulder, elbow, and wrist joints
 * @param skeleton
 * @return the absolute value of the angle
 */
float getangle(k4abt_skeleton_t skeleton){

    k4abt_joint_t shoulder_left = skeleton.joints[jointname[2]];
    k4abt_joint_t elbow_left = skeleton.joints[jointname[3]];
    k4abt_joint_t wrist_left = skeleton.joints[jointname[4]];
    k4abt_joint_t shoulder_right = skeleton.joints[jointname[5]];
    k4abt_joint_t elbow_right = skeleton.joints[jointname[6]];
    k4abt_joint_t wrist_right = skeleton.joints[jointname[7]];

    float a2 = computeAngle(shoulder_right, elbow_right, wrist_right);


    return abs(a2);

}






/**
 * Compare the frame captured by kinect and the frame recorded by a coach as reference
 * @param skeleton skeleton captured by kinect
 * @param referenceSkeleton skeleton get in database, the reference
 * @return true if the two skeletons matched, false otherwise
 */
bool compareFrame(k4abt_skeleton_t skeleton, k4abt_skeleton_t referenceSkeleton)
{

    // Get list of angle for both skeletons
    std::vector<float> skeletonAnglelist = getAngles(skeleton) ;
    std::vector<float> skeletonRefrenceAnglelist = getAngles(referenceSkeleton);

    // If all angle matched (with an error margin) return true
    for(int i = 0 ; i< skeletonAnglelist.size();i++)
    {
        if(abs((skeletonRefrenceAnglelist[i] - skeletonAnglelist[i]) > 15))
        {
            return false;
        }
    }
    return true;
}

/**
 * detect if the position of the user is the initial position of the movement
 * @param skeleton skeleton captured by kinect
 * @param referenceSkeleton skeleton of coach the reference
 * @return true if skeleton is in initial position, false otherwise
 */
bool detectInitialPosition(k4abt_skeleton_t skeleton, k4abt_skeleton_t referenceSkeleton)
{
    // Simply a comparison between skeleton captured by kinect and the first skeleton of the movement referenced
    if(compareFrame(skeleton, referenceSkeleton))
    {
        std::cout << "\nExercise started ! " << std::endl;
        return true;
    }

    return false;
}

/**
 *
 * @return the smallest angle of a movement
 */
float getMinAngle() {

    Movement m = getMovementSkeletons("curl");
    float min = 999;


    for (int i = 0; i < m.getSkeletons().size(); i++) {

        if (min > abs (getangle(m.getSkeletons()[i]))) min = getangle(m.getSkeletons()[i]);
    }
    return min ;


}
/**
 *
 * @param angleDiffrence
 * @return a score based on the angle difference between the users angle and the one saved in the data base
 */
string getResult(float angleDiffrence){
    string message ;
    if(angleDiffrence < 5) message = "perfect movement";
    else if (angleDiffrence < 15) message = "not bad ";
    else if (angleDiffrence < 30) message = "average ";
    else message = "you are bad ";

    return  message;
}

/**
 *
 * @return the time that took to do the movement curl
 */
float getTime(){
    Movement m = getMovementSkeletons("curl");

    int frames = m.getSkeletons().size();

    return frames/15;

}

/**
 *
 * @param frame
 * @return a score based on the time that it took to do a movement
 */
string getTimeResult(int frame){
    float time = frame/15;
    float timeRefrence = getTime();
    string message;
    float z = abs(timeRefrence - time);

    if(z<0.8) message = "good timing " ;
    else if(z<1.3 ) message = "not bad  timing ";
    else if  (z<5) message = "bad timing";
    else message = "horrible timing";
    return  message;
}