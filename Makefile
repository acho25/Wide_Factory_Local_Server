CC = g++
FLAG = -flto -O3 -lpthread -Wpedantic -Wall -Wextra -Wsign-conversion -Wconversion -std=c++2a
LIB = -lz -lk4a -lk4abt -ljsoncpp -lpqxx -lpq
USOCKETS = -I /usr/local/include/uSockets /usr/local/include/uSockets/uSockets.a
OBJ = server.o kinect.o movement.o databaseManager.o comparison.o



all: $(OBJ)
	$(CC) -o server.exe $^ $(FLAG) $(LIB) $(USOCKETS)



server.o: server.cpp
	$(CC) -c $(FLAG) $< -o $@ $(USOCKETS)

kinect.o: kinect.cpp kinect.h
	$(CC) -c $(FLAG) $< -o $@

movement.o: movement.cpp movement.h
	$(CC) -c $(FLAG) $< -o $@

databaseManager.o: databaseManager.cpp databaseManager.h
	$(CC) -c $(FLAG) $< -o $@

comparison.o: comparison.cpp comparison.h
	$(CC) -c $(FLAG) $< -o $@



clean:
	rm -f *o

erase:
	make clean
	rm -f *.exe