#!/bin/bash

# Install postgreSQL libraries
sudo apt-get install libpq-dev postgresql-server-dev-all

# Install libpqxx
git clone git@github.com:jtv/libpqxx.git
cd libpqxx
cmake .
cmake --build .
sudo cmake --install .
cd ..
rm -rf libpqxx
