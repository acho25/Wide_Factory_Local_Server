#!/bin/bash

git clone --recurse-submodules https://github.com/uNetworking/uWebSockets
cd uWebSockets/
sudo make install

cd uSockets/
make
sudo mkdir /usr/local/include/uSockets
sudo cp -r ./src/* /usr/local/include/uSockets
sudo cp uSockets.a /usr/local/include/uSockets

cd ../..
rm -rf uWebSockets/