# Installation procedures

This file explains how install and use the dependencies for this project (server side).  
You must have admin right to install the libraries


### If you want to install all tools needed directly you can execute **install_all_tools.sh** script  


## Install and use uWebsockets / uSockets

**This installation is automated by "install_uWebSocket.sh"** script.  
uWebsockets is used to run a websocket server, it depends on uSockets.  
The two libraries will be installed in */usr/local/include*

```bash
git clone --recurse-submodules https://github.com/uNetworking/uWebSockets
cd uWebSockets/
sudo make install

cd uSockets/
make
sudo mkdir /usr/local/include/uSockets
sudo cp -r ./src/* /usr/local/include/uSockets
sudo cp uSockets.a /usr/local/include/uSockets

cd ../..
rm -rf uWebSockets/
```

### A main example

When uWebsocket and uSockets are installed you can try with a main example.  
*server.cpp*

```c++
#include <uWebSockets/App.h>

int main()
{
    /* ws->getUserData returns one of these */
    struct UserData
    {
        /* Define your user data */
        int something;
    };

    uWS::App().get("/monTest", [](auto*res, auto*req)
    {
        /* You can efficiently stream huge files too */
        res->writeHeader("Content-Type", "text/html; charset=utf-8")->end("Hello HTTP!");
    }).ws<UserData>("/monTest", {

        /* Just a few of the available handlers */
        .open = [](auto*ws)
        {
            /* MQTT syntax */
            ws->subscribe("sensors/+/house");
            std::cout << "WS open" << std::endl;
        },

        .message = [](auto*ws, std::string_view message, uWS::OpCode opCode)
        {
            ws->send("message from uWebSocket", opCode);
            std::cout<<message<<std::endl;
        },

        .close = [](auto */*ws*/, int /*code*/, std::string_view /*message*/)
        {
            std::cout << "WS closed" << std::endl;
        }
    }).listen(9001, [](auto*listenSocket) {

        if (listenSocket)
        {
            std::cout<<"Listening on port "<<9001<<std::endl;
        }
    }).run();
}
```

Then compile the example with this Makefile

```Makefile
CC = g++
FLAG = -flto -O3 -lpthread -Wpedantic -Wall -g -Wextra -Wsign-conversion -Wconversion -std=c++2a
LIB = -lz
USOCKETS = -I /usr/local/include/uSockets /usr/local/include/uSockets/uSockets.a


all:
	$(CC) -o server.exe server.cpp $(FLAG) $(USOCKETS) $(LIB)
```
Run the main test with : ./server.exe



## Install and use libpqxx

libpqxx is a postgreSQL client in C++.  
before you must install the postgresSQL libraries.  
**This installation is automated by "install_libpqxx.sh"** script.  

```bash
sudo apt-get install libpq-dev postgresql-server-dev-all
```

Download the lipqxx project, build it and install it

```bash
git clone git@github.com:jtv/libpqxx.git
cd libpqxx
cmake .
cmake --build .
sudo cmake --install .

cd ..
rm -rf libpqxx
```

### A main example

When libpqxx is installed try with this main example "main.cpp"

```c++
#include <iostream>
#include <pqxx/pqxx>

int main(int, char *argv[])
{
    // hostaddr = IP of host than run postgreSQL
    pqxx::connection c{"dbname=test password=uwu user=postgres hostaddr=172.17.0.2"};
    pqxx::work txn{c};
    
    txn.exec0("CREATE TABLE PERSONNE ( NOM VARCHAR(20), PRIMARY KEY(NOM) )");
    
    // Make our change definite.
    txn.commit();
    
    return 0;
}
```

It creates a table PERSONNE in the database "test" with the user "postgres" password "uwu" the database is in 172.17.0.2

Compile the example with this command 
```bash
g++ main.cpp -lpqxx -lpq -std=c++2a
```



## Install JSONCPP

JSONCPP is used to parse string in JSON

```bash
sudo apt-get install libjsoncpp-dev
```


## Install Kinect Body Tracking SDK

```bash
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo apt-add-repository https://packages.microsoft.com/ubuntu/18.04/prod
curl -sSL https://packages.microsoft.com/config/ubuntu/18.04/prod.list | sudo tee /etc/apt/sources.list.d/microsoft-prod.list
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo apt-get update
sudo apt install libk4a1.3-dev
sudo apt install libk4abt1.0-dev
sudo apt install k4a-tools=1.3.0
```
