#include <k4a/k4a.h>
#include <k4abt.h>
#include <iostream>
#include <vector>
#include <memory>
#include<jsoncpp/json/writer.h>

#include "movement.h"

//default constructor and destructor

Movement::Movement(){

}

Movement::~Movement(){

}

std::vector<k4abt_skeleton_t> Movement::getSkeletons(){
  return this->skeletons;
}

void Movement::addSkeleton(k4abt_skeleton_t ske){
  this->skeletons.push_back(ske);
}


/*
  Convert a Moevement object into a string that represent a valid JSON files
  offset is in case the Movement object itself is not the full movement and we need multiple, to sync the frame count we use the offset
*/
std::string Movement::convertToJson(int offset = 0){

    int nbFrame = offset;

  //json values for to get skeletons of movements (root)
  Json::Value skeleton;
  Json::Value frames(Json::arrayValue);
  Json::Value jsonData(Json::arrayValue);
  //Json::Value joints(Json::arrayValue);

  // json value to get coordinates and quaterions for a joint
  Json::Value joint;

  //Building the json file
  Json::StreamWriterBuilder builder;
  builder["commentStyle"] = "None";
  builder["indentation"] = "   ";
  std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
  Json::FastWriter fastWriter;

  for (k4abt_skeleton_t sk : this->skeletons){

      nbFrame++;
      skeleton.clear();
      skeleton["frame"] = nbFrame;


    for (int i = 0; i < 32; i++) {

      k4abt_joint_t j = sk.joints[i];
      k4a_quaternion_t quat = j.orientation;
      k4a_float3_t position = j.position;

      //insert the joint's name , cordinates , quaterions into the event json value
      joint["name"] = this->jointname[i];
      joint["x"] = position.xyz.x;
      joint["y"] = position.xyz.y;
      joint["z"] = position.xyz.z;
      joint["w"] = quat.wxyz.w;
      joint["wx"] = quat.wxyz.y;
      joint["wy"] = quat.wxyz.z;
      joint["wz"] = quat.wxyz.x;

      //Save these informations (name , coordinates , quaterions) in a table
      frames.append(joint);
    }

    skeleton["joints"] = frames;
    jsonData.append(skeleton);
    frames.clear();
  }

  std::string ret = fastWriter.write(jsonData);

  return ret;

}




















//
