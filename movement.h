#ifndef MOVEMENT_H
#define MOVEMENT_H

#include <k4a/k4a.h>
#include <k4abt.h>
#include <vector>


/*
  Class that represent a Movement, it's store in a collection (vector) of skeletons
*/
class Movement{
private:
  //list of joint's name
  std::string jointname[33] =
          {
                  "K4ABT_JOINT_PELVIS",
                  "K4ABT_JOINT_SPINE_NAVEL",
                  "K4ABT_JOINT_SPINE_CHEST",
                  "K4ABT_JOINT_NECK",
                  "K4ABT_JOINT_CLAVICLE_LEFT",
                  "K4ABT_JOINT_SHOULDER_LEFT",
                  "K4ABT_JOINT_ELBOW_LEFT",
                  "K4ABT_JOINT_WRIST_LEFT",
                  "K4ABT_JOINT_HAND_LEFT",
                  "K4ABT_JOINT_HANDTIP_LEFT",
                  "K4ABT_JOINT_THUMB_LEFT",
                  "K4ABT_JOINT_CLAVICLE_RIGHT",
                  "K4ABT_JOINT_SHOULDER_RIGHT",
                  "K4ABT_JOINT_ELBOW_RIGHT",
                  "K4ABT_JOINT_WRIST_RIGHT",
                  "K4ABT_JOINT_HAND_RIGHT",
                  "K4ABT_JOINT_HANDTIP_RIGHT",
                  "K4ABT_JOINT_THUMB_RIGHT",
                  "K4ABT_JOINT_HIP_LEFT",
                  "K4ABT_JOINT_KNEE_LEFT",
                  "K4ABT_JOINT_ANKLE_LEFT",
                  "K4ABT_JOINT_FOOT_LEFT",
                  "K4ABT_JOINT_HIP_RIGHT",
                  "K4ABT_JOINT_KNEE_RIGHT",
                  "K4ABT_JOINT_ANKLE_RIGHT",
                  "K4ABT_JOINT_FOOT_RIGHT",
                  "K4ABT_JOINT_HEAD",
                  "K4ABT_JOINT_NOSE",
                  "K4ABT_JOINT_EYE_LEFT",
                  "K4ABT_JOINT_EAR_LEFT",
                  "K4ABT_JOINT_EYE_RIGHT",
                  "K4ABT_JOINT_EAR_RIGHT",
                  "K4ABT_JOINT_COUNT"
          } ;

  std::vector<k4abt_skeleton_t> skeletons; //our skeletons collection
public:
  Movement();
  ~Movement();

  std::vector<k4abt_skeleton_t> getSkeletons();
  void addSkeleton(k4abt_skeleton_t ske);

  std::string convertToJson(int offset);

};

#endif
