#include "databaseManager.h"

/** Settings connection with database */
connection conn("dbname=wf user=postgres password=test hostaddr=172.17.0.2 port=5432");
nontransaction trans(conn);


/**
 * Convert varchar of joints name to position in enum
 * @param s the joint name
 * @return an enum that correspond of this string needed by joint object
 */
k4abt_joint_id_t getEnumIndex(const string s)
{
    // Used to map string to enum
    static std::map<std::string,k4abt_joint_id_t> string2joint
    {
            { "K4ABT_JOINT_PELVIS", K4ABT_JOINT_PELVIS },
            { "K4ABT_JOINT_SPINE_NAVEL", K4ABT_JOINT_SPINE_NAVEL },
            { "K4ABT_JOINT_SPINE_CHEST", K4ABT_JOINT_SPINE_CHEST },
            { "K4ABT_JOINT_NECK", K4ABT_JOINT_NECK },
            { "K4ABT_JOINT_CLAVICLE_LEFT", K4ABT_JOINT_CLAVICLE_LEFT },
            { "K4ABT_JOINT_SHOULDER_LEFT", K4ABT_JOINT_SHOULDER_LEFT },
            { "K4ABT_JOINT_ELBOW_LEFT", K4ABT_JOINT_ELBOW_LEFT },
            { "K4ABT_JOINT_WRIST_LEFT", K4ABT_JOINT_WRIST_LEFT },
            { "K4ABT_JOINT_HAND_LEFT", K4ABT_JOINT_HAND_LEFT },
            { "K4ABT_JOINT_HANDTIP_LEFT", K4ABT_JOINT_HANDTIP_LEFT },
            { "K4ABT_JOINT_THUMB_LEFT", K4ABT_JOINT_THUMB_LEFT, },
            { "K4ABT_JOINT_CLAVICLE_RIGHT", K4ABT_JOINT_CLAVICLE_RIGHT },
            { "K4ABT_JOINT_SHOULDER_RIGHT", K4ABT_JOINT_SHOULDER_RIGHT },
            { "K4ABT_JOINT_ELBOW_RIGHT", K4ABT_JOINT_ELBOW_RIGHT },
            { "K4ABT_JOINT_WRIST_RIGHT", K4ABT_JOINT_WRIST_RIGHT },
            { "K4ABT_JOINT_HAND_RIGHT", K4ABT_JOINT_HAND_RIGHT },
            { "K4ABT_JOINT_HANDTIP_RIGHT", K4ABT_JOINT_HANDTIP_RIGHT },
            { "K4ABT_JOINT_THUMB_RIGHT", K4ABT_JOINT_THUMB_RIGHT },
            { "K4ABT_JOINT_HIP_LEFT", K4ABT_JOINT_HIP_LEFT },
            { "K4ABT_JOINT_KNEE_LEFT", K4ABT_JOINT_KNEE_LEFT },
            { "K4ABT_JOINT_ANKLE_LEFT", K4ABT_JOINT_ANKLE_LEFT },
            { "K4ABT_JOINT_FOOT_LEFT", K4ABT_JOINT_FOOT_LEFT },
            { "K4ABT_JOINT_HIP_RIGHT", K4ABT_JOINT_HIP_RIGHT },
            { "K4ABT_JOINT_KNEE_RIGHT", K4ABT_JOINT_KNEE_RIGHT },
            { "K4ABT_JOINT_ANKLE_RIGHT", K4ABT_JOINT_ANKLE_RIGHT },
            { "K4ABT_JOINT_FOOT_RIGHT", K4ABT_JOINT_FOOT_RIGHT },
            { "K4ABT_JOINT_HEAD", K4ABT_JOINT_HEAD },
            { "K4ABT_JOINT_NOSE", K4ABT_JOINT_NOSE },
            { "K4ABT_JOINT_EYE_LEFT", K4ABT_JOINT_EYE_LEFT },
            { "K4ABT_JOINT_EAR_LEFT", K4ABT_JOINT_EAR_LEFT },
            { "K4ABT_JOINT_EYE_RIGHT", K4ABT_JOINT_EYE_RIGHT },
            { "K4ABT_JOINT_EAR_RIGHT", K4ABT_JOINT_EAR_RIGHT },
            { "K4ABT_JOINT_COUNT", K4ABT_JOINT_COUNT },
    };


    auto x = string2joint.find(s);
    if(x != end(string2joint))
    {
        return x->second;
    }

    throw invalid_argument("s");
}

/**
 * Get a skeleton with its joints from database with this name
 * @param idSkeleton ID of the skeleton wanted
 * @return a skeleton object
 */
k4abt_skeleton_t getJointsSkeleton(int idSkeleton)
{
    k4abt_skeleton_t skeleton;

    try
    {
        // Create SQL statement
        string query = "SELECT * from JOINT WHERE id_skeleton = '" + to_string(idSkeleton) + "'";

        // Execute SQL query
        result R(trans.exec(query));

        /* List down all the records */
        for (result::const_iterator c = R.begin(); c != R.end(); ++c)
        {
            // Set the joints of the skeleton with the data retrieved
            k4abt_joint_id_t position = getEnumIndex(c["name"].as<string>());
            skeleton.joints[position].position.xyz.x = c["x"].as<float>();
            skeleton.joints[position].position.xyz.y = c["y"].as<float>();
            skeleton.joints[position].position.xyz.z = c["z"].as<float>();
            skeleton.joints[position].orientation.wxyz.w = c["w"].as<float>();
            skeleton.joints[position].orientation.wxyz.x= c["wx"].as<float>();
            skeleton.joints[position].orientation.wxyz.y= c["wy"].as<float>();
            skeleton.joints[position].orientation.wxyz.z= c["wy"].as<float>();
        }
    }
    catch (const std::exception &e)
    {
        cerr << e.what() << std::endl;
    }

    return skeleton;
}

/**
 * Get a movement with its skeletons from database by his name
 * @param NAME_MOVEMENT name of movement wanted
 * @return a movement object
 */
Movement getMovementSkeletons(string NAME_MOVEMENT)
{
    Movement movement = Movement();
    int idSkeleton;

    try
    {
        // Create SQL statement
        // Sort by frame number to get the right order of movement
        string query = "SELECT * from SKELETON WHERE NAME_MOVEMENT = '" + NAME_MOVEMENT +  "' ORDER BY FRAME ASC";

        // Execute SQL query
        result R(trans.exec(query));

        // Add skeletons to the movement
        for (result::const_iterator c = R.begin(); c != R.end(); ++c)
        {
            // Get skeleton id that correspond to the movement order (example frame number 1 --> id 8)
            // Id skeleton is not the movement order, it's frame number that describe movement order
            idSkeleton = c["id"].as<int>();
            movement.addSkeleton(getJointsSkeleton(idSkeleton));
        }
    }
    catch (const std::exception &e)
    {
        cerr << e.what() << std::endl;
    }

    return movement;
}
