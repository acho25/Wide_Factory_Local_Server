#ifndef WIDE_FACTORY_SERVEUR_LOCAL_DATABASEMANAGER_H
#define WIDE_FACTORY_SERVEUR_LOCAL_DATABASEMANAGER_H

#include <iostream>
#include <string>
#include <pqxx/pqxx>
#include "movement.h"
#include "k4abttypes.h"

using namespace std;
using namespace pqxx;

/** Set of methods which enable to get data from database and transform it in object */

k4abt_joint_id_t getEnumIndex(const string s);
k4abt_skeleton_t getJointsSkeleton(int ID_SKELETON);
Movement getMovementSkeletons(string NAME_MOVEMENT);

#endif
