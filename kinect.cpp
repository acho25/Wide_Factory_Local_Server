#include <k4a/k4a.h>
#include <k4abt.h>

#include <iostream>

#include "kinect.h"

/*
	init of the kinect
*/
Kinect::Kinect(k4a_device_t kinect, k4abt_tracker_t tracker){
	this->kinect = kinect;
	this->tracker = tracker;

}

/*
	we release the kinect for future connection
*/
Kinect::~Kinect(){
	//free
	k4abt_tracker_shutdown(this->tracker);
  k4abt_tracker_destroy(this->tracker);
  k4a_device_stop_cameras(this->kinect);
  k4a_device_close(this->kinect);

}

/*
	return the skeleton of the current frame

	!!! the kinect on it's own doesn't hold a buffer so it's up to us to get those fast enough to not get frameskips
*/
k4abt_skeleton_t Kinect::captureFrame(){

	k4a_capture_t sensor_capture;
	k4a_wait_result_t get_capture_result = k4a_device_get_capture(this->kinect, &sensor_capture, K4A_WAIT_INFINITE);

	if (get_capture_result == K4A_WAIT_RESULT_SUCCEEDED){

		k4a_wait_result_t queue_capture_result = k4abt_tracker_enqueue_capture(this->tracker, sensor_capture, K4A_WAIT_INFINITE);//we add a new capture to the input queue
		k4a_capture_release(sensor_capture); // Remember to release the sensor capture once you finish using it

		if (queue_capture_result == K4A_WAIT_RESULT_TIMEOUT){ //check if queue capture result hit an end
		    // It should never hit timeout when K4A_WAIT_INFINITE is set.
		    
		    std::cout << "Error! Add capture to tracker process queue timeout!" << std::endl;
		    
		    exit(1);
		}
		else if (queue_capture_result == K4A_WAIT_RESULT_FAILED){ //check if the capture result failed
		    std::cout << "Error! Add capture to tracker process queue failed!" << std::endl;
		    exit(1);
		}

		k4abt_frame_t body_frame = NULL;
		k4a_wait_result_t pop_frame_result = k4abt_tracker_pop_result(this->tracker, &body_frame, K4A_WAIT_INFINITE);

		if (pop_frame_result == K4A_WAIT_RESULT_SUCCEEDED){
		    // Successfully popped the body tracking result. Start your processing

		    num_bodies = k4abt_frame_get_num_bodies(body_frame);
		    // printf("%zu bodies are detected!\n", num_bodies);

		    k4abt_skeleton_t skeleton;
		    k4a_result_t bodyResult = k4abt_frame_get_body_skeleton(body_frame, 0, &skeleton);

		    k4abt_frame_release(body_frame); // Remember to release the body frame once you finish using it

				if (bodyResult == K4A_RESULT_SUCCEEDED){ //everything good we send back the skeleton
					return skeleton;
				}
		}

		else if (pop_frame_result == K4A_WAIT_RESULT_TIMEOUT){
		    //  It should never hit timeout when K4A_WAIT_INFINITE is set.
		    		    std::cout << "Error! Pop body frame result timeout!" << std::endl;
		    exit(1);
		}
		else{
		    		    std::cout << "Pop body frame result failed!" << std::endl;
		    exit(1);
		}
	}
	else if (get_capture_result == K4A_WAIT_RESULT_TIMEOUT){
	    // It should never hit time out when K4A_WAIT_INFINITE is set.
	    		    std::cout << "Error! Get depth frame time out!" << std::endl;
	    exit(1);
	}
	else{
	    std::cout << "Get depth capture returned error: " << get_capture_result << std::endl;
	    exit(1);
	    

	}

	//here compilation warning, we can't reach this part ever, we either return early or call exit so warning dealt with
	
}

/*
	wrapper around the constructor to only return the Kinect if the init work correctly
*/
Kinect* Kinect::kinectFactory(){

	k4a_device_t device = NULL;

    //check if the device is connected
    VERIFY(k4a_device_open(0, &device), "Open K4A Device failed!");

    // Start camera. Make sure depth camera is enabled.
    k4a_device_configuration_t deviceConfig = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
	deviceConfig.camera_fps = K4A_FRAMES_PER_SECOND_15;  
    deviceConfig.depth_mode = K4A_DEPTH_MODE_NFOV_UNBINNED;
    deviceConfig.color_resolution = K4A_COLOR_RESOLUTION_OFF;
    VERIFY(k4a_device_start_cameras(device, &deviceConfig), "Start K4A cameras failed!");
    k4a_calibration_t sensor_calibration;
    VERIFY(k4a_device_get_calibration(device, deviceConfig.depth_mode, deviceConfig.color_resolution, &sensor_calibration),
           "Get depth camera calibration failed!");

    //check if the tracker is working properly
    k4abt_tracker_t tracker = NULL;
    k4abt_tracker_configuration_t tracker_config = K4ABT_TRACKER_CONFIG_DEFAULT;
    VERIFY(k4abt_tracker_create(&sensor_calibration, tracker_config, &tracker), "Body tracker initialization failed!");

    return new Kinect(device, tracker);
}


size_t Kinect::getNumBodies(){
    return num_bodies;
}